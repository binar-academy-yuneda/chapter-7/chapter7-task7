import React, { useState } from "react";

import ExpenseForm from "./ExpenseForm";
import "./NewExpense.css";

const NewExpense = (props) => {
  const [isAddNewData, setIsNewAddData] = useState(false);
  const saveExpenseDataHandler = (enteredExpenseData) => {
    const expenseData = {
      ...enteredExpenseData,
      id: Math.random().toString(),
    };
    console.log("di new expense");
    console.log(expenseData);
    props.onAddExpense(expenseData);
  };
  const startAddNewData = () => {
    setIsNewAddData(true);
  };

  const cancelAddNewData = () => {
    setIsNewAddData(false);
  };
  return (
    <div className="new-expense">
      {!isAddNewData && (
        <button onClick={startAddNewData}>Add New Expense</button>
      )}
      {isAddNewData && (
        <ExpenseForm
          passingCancel={cancelAddNewData}
          onSaveExpenseData={saveExpenseDataHandler}
        />
      )}
    </div>
  );
};

export default NewExpense;
