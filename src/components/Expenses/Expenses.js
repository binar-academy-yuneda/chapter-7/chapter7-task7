import React, { useState } from "react";

import ExpenseItem from "./ExpenseItem";
import Card from "../UI/Card";
import "./Expenses.css";
import ExpensesFilter from "./ExpensesFilter";
import ExpensesList from "./ExpensesList";
import ExpensesChart from "./ExpensesChart";

const Expenses = (props) => {
  const [year, setYear] = useState("all");

  const filterHandler = (selectedYear) => {
    setYear(selectedYear);
  };

  let filterExpense = props.items;
  if (year !== "all") {
    filterExpense = props.items.filter((expense) => {
      return expense.date.getFullYear().toString() === year;
    });
  }

  return (
    <Card className="expenses">
      <ExpensesChart expenses={filterExpense} />
      <ExpensesFilter selectedYear={year} onChangeFilter={filterHandler} />
      <ExpensesList items={filterExpense} />
    </Card>
  );
};

export default Expenses;
