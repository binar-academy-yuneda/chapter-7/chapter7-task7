import React from "react";
import "./ExpensesList.css";
import ExpenseItem from "./ExpenseItem";

const ExpensesList = ({ items }) => {
  let expenseContent = <p>No Expense found</p>;
  if (items.length > 0) {
    expenseContent = items.map((item) => {
      return (
        <ExpenseItem
          key={item.id}
          title={item.title}
          amount={item.amount}
          date={item.date}
        />
      );
    });
  }
  return expenseContent;
};

export default ExpensesList;
